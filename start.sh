#if [[ "$(docker info 2>&1| grep Swarm | sed 's/Swarm: //g')" == "inactive" ]]; then docker swarm init --advertise-addr eth0; fi

#mkdir -p /etc/prometheus
#mkdir -p /etc/alertmanager
#mkdir -p /etc/grafana/db

#cp -f prometheus/prometheus.yml /etc/prometheus/
#cp -f alertmanager/config.yml /etc/alertmanager/
#cp -f prometheus/alert.rules.yml /etc/prometheus/
#cp -f grafana.db /etc/grafana/db
cp -Rf prometheus /etc/
cp -Rf alertmanager /etc/
cp -Rf grafana /etc/

chmod -R 777 /etc/prometheus
chmod -R 777 /etc/alertmanager
chmod -R 777 /etc/grafana

docker rm -f prom alert grafana

docker run -d -p 9090:9090 --name prom \
 -v /etc/prometheus:/prometheus \
 -v /etc/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml \
 -v /etc/prometheus/alert.rules.yml:/etc/prometheus/alert.rules.yml \
 prom/prometheus

docker run -d -p 9093:9093 --name alert \
 -v /etc/alertmanager/config.yml:/etc/alertmanager/config.yml \
 prom/alertmanager '--config.file=/etc/alertmanager/config.yml'

docker run -d -p 3000:3000 --name grafana \
 -v /etc/grafana/:/var/lib/grafana/ \
 -e "GF_INSTALL_PLUGINS=grafana-piechart-panel,briangann-gauge-panel" \
 grafana/grafana
 #-v /etc/grafana/db:/var/lib/grafana \
 #-v /etc/grafana/grafana.ini:/etc/grafana/grafana.ini \
 
 
# Create promgen setting directory.
#mkdir /etc/promgen
#chmod 777 /etc/promgen

# Initialize required settings with Docker container
# This will prompt you for connection settings for your database and Redis broker
# using the standard DSN syntax.
# Database example: mysql://username:password@hostname/databasename
# Broker example: redis://localhost:6379/0
#docker run --rm -it -v /etc/promgen:/etc/promgen/ line/promgen bootstrap

# Apply database updates
#docker run --rm -v /etc/promgen:/etc/promgen/ line/promgen migrate

# Create initial login user. This is the same as the default django-admin command
# https://docs.djangoproject.com/en/1.10/ref/django-admin/#django-admin-createsuperuser
#docker run --rm -it -v /etc/promgen:/etc/promgen/ line/promgen createsuperuser

#template grafana
#2129

#firing an test alert
#curl -d '[{"labels": {"Alertname": "PagerDuty Test"}}]' http://localhost:9093/api/v1/alerts