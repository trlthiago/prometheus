A estrutura de monitoramento do APP (prometheus + Elastic) está hospedada em uma VM no E29.
```
VM Name: UBAPM01
IP: 177.55.119.91
GW: 177.55.119.65 
Subnet: 177.55.119.64/26
VLAN: 120
```

Obs: este IP está dentro de um range de CloudSite e deve ser trocado no futuro. 
Quando isso for feito, tem-se que ajustar:
```
- DNS's (infra.app.umbler.com / apm.umbler.com)
- IPSec (regra PROMETHEUS)
```

O VHDX está dimensionado para 200GB. Se precisar aumentar o disco, usar:

```
Get-SCVirtualMachine ubapm01 |  Get-SCVirtualDiskDrive | Expand-SCVirtualDiskDrive -VirtualHardDiskSizeGB 200
```

Obs: o comando acima considera que a máquina tem somente um vhdx. Se tiver mais de um vhdx ele vai expandir todos. Ou deve-se ajustar a linha de comando para pegar somente o vhdx desejado.

Dentro do OS, temos os seguintes comandos para expandir:
```
$ lvextend -L+30G /dev/mapper/ubuntu--vg-ubuntu--lv
$ resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv
```
-------------------------------

## Subir o prometheus (METODO ANTIGO):
1. Clonar este repo
2. chmod +x start.sh
3. ./start.sh 

## Subir o prometheus (METODO NOVO):
1. cd /root
2. git clone [este repo]
3. chmod 777 prometheus -R
4. cd prometheus 
5. docker-compose up -d

## Subir o Elastic:
1. cd /root
2. git clone git@gitlab.com:umbler/infra/elasticapm.git
3. copiar o certificado da umbler para /root/certs 
4. docker-compose up -d

-------------------------------

## Backup
Precisamos bolar o backup desse sistema. Apesar da grande maioria das configurações estarem feitas via arquivo de configuração e tais estarem versionados neste repositório, o grafana mantem um banco de dados SQLite, que precisa ser feito backup.

## Quem pode dar mais informações:
- Thiago Roberto
- Marcos Artigas
- https://awesome-prometheus-alerts.grep.to/rules.html